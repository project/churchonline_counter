ChurchOnline Counter 1.x for Drupal 7.x
---------------------------------------

This module implements the jQuery coundown embed code from Church Online
Platform. http://churchonlineplatform.com/

You will need your Church Online Platform domain name to configure
the block.
